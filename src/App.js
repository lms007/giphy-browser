import React, { Component } from 'react';
import { connect } from 'react-redux'
import GiphyResults from './GiphyResults';
import SearchBar from './SearchBar';

/*
The default query is the trending URL and if search text is detected
from the search bar, then it changes to the search URL. The first 
fetch is limited to 100 items, and each subsequent fetch (lazy load) is 25.
*/
class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
        url: this.props.url,
        options: {...this.props.options} // always make a copy here to prevent mutation
    }
  }

  switchToTrending() {
      this.setState({
          url: 'https://api.giphy.com/v1/gifs/trending?',
          options: {
            ...this.state.options,
            offset: 0,
          }
      })
  }

  switchToSearch(term) {
      this.setState({
          url: 'https://api.giphy.com/v1/gifs/search?',
          options: {
            ...this.state.options,
            offset: 0,
            q: term,
          }
      })
  }

  handleChange(value) {
    if (value.length > 0) {
      this.switchToSearch(value);
    }
    else {
      this.switchToTrending();
    }
  }

  render() {
    return (
      <div className="App">
        <SearchBar onChange={this.handleChange.bind(this)}/>
        <GiphyResults url={this.state.url} options={this.state.options} nextFetchLimit={25} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    ...state.fetchReducer,
  }
}

export default connect(
  mapStateToProps
)(App)
