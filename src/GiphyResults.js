import React, { Component } from 'react';
import { connect } from 'react-redux'
import './GiphyResults.css';
import Masonry from 'react-masonry-component';
import {fetchAction} from './redux/actions';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';
import PropTypes from 'prop-types';

/*
Renders gif results in a masonry grid. Clicking on each result
will open a light gif with the original gif. Scrolling
down the page will load more results if there are any more from
the service.
*/

class GiphyResults extends Component {

  constructor(props) {
    super(props);
    this.state = {
      checkDelay: 0, // throttles the scrolling a little
      loadedImages: [], // an array to keep track of what loaded so we can animate
      isComplete: false, // last known complete state
      showDetails: false,
      detailsItem: {},
      fetched: props.options.limit,
    };
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll.bind(this));
    // on initial load, fetch the default query
    fetchAction(this.props.url, this.props.options);
  }

  componentWillUnmount() {
      window.removeEventListener('scroll', this.handleScroll);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.url !== this.props.url || prevProps.options.q !== this.props.options.q) {
      window.scrollTo(0, 0); // this is a new set of props, scroll back to the top
      this.setState({
        ...this.state,
        loadedImages: [],
        isComplete: false,
        fetched: this.props.options.limit,
      });
      // fetch the new query
      fetchAction(this.props.url, this.props.options);
    }
  }

  handleScroll(event) {
    const currentY = window.pageYOffset;
    // trigger a new request if we reach the bottom
    if (currentY > document.body.clientHeight - window.outerHeight) {
      const currentTime = new Date().getTime();
      // adds a little delay to the scroll check so we don't fire off 2 events at once
      if (this.state.checkDelay < currentTime) {
        // first check to see if we have anymore more results to fetch from the server
        if (this.props.resultsReducer.results[this.props.resultsReducer.results.length-1].pagination.total_count >
          this.state.fetched) {
          fetchAction(this.props.url, {
            limit: this.props.nextFetchLimit,
            offset: this.state.fetched,
          });
          this.setState({
            fetched: this.state.fetched + this.props.nextFetchLimit,
            isComplete: false,
            checkDelay: currentTime + 100 // delay in (ms) for the next check
          })
        }
      }
    }
  }
  /*
  Helper that add width and height to the img tag which causes
  a css animation once its fully loaded.
  */
  handleImagesLoaded(event) {
    var loadedImages = event.images.map((loading => {
      return loading.isLoaded === true
    }));
    // check to see if we have some new images to update?
    if (this.state.loadedImages.length !== event.images.length ||
      !event.isComplete || !this.state.isComplete) {
      this.setState({
          ...this.state,
          isComplete: event.isComplete,
          loadedImages
      })
    }
  }
  /*
  Opens the light box
  */
  showDetails(item) {
    this.setState({
      ...this.state,
      showDetails: true,
      detailsItem: item
    })
  }

  render() {
    const resultsArray = this.props.resultsReducer.results;
    const Gifs = [];
    let totalCount = 0;
    // outer loop is chunked results from server
    for (var j = 0; j < resultsArray.length; j++) {
      const results = resultsArray[j];
      // inner loop is each gif object in a result chunk
      for(var i = 0; i < results.data.length; i++) {
        const gifItem = results.data[i];
        let x = '0px';
        let y = '0px';
        if (this.state.loadedImages[totalCount] === true) {
          // this image is loaded, add the dimensions
          x = gifItem.images.fixed_width.width + 'px';
          y = gifItem.images.fixed_width.height + 'px';
        }
        const Gif = (
          <span key={`gifs-${totalCount}`} className="gif-wrapper"
            style={{
              width: gifItem.images.fixed_width.width + 'px',
              height: gifItem.images.fixed_width.height + 'px',
            }}
          >
            <img
              onClick={this.showDetails.bind(this, gifItem)}
              src={gifItem.images.fixed_width.url}
              alt={gifItem.display_name}
              style={{
                  width: x,
                  height: y
              }}
            />
          </span>);
        Gifs.push(Gif);
        totalCount += 1;
      }
    }
    return (
      <div className="gif-gallery">
        {this.state.showDetails && (
          <Lightbox
            mainSrc={this.state.detailsItem.images.original.url}
            onCloseRequest={() => this.setState({ ...this.state, showDetails: false })}
            imageTitle={this.state.detailsItem.title}
          />
        )}
        <Masonry
          updateOnEachImageLoad={true}
          onImagesLoaded={this.handleImagesLoaded.bind(this)}
          options={{
            transitionDuration: 300
          }}
        >{Gifs}</Masonry>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    resultsReducer: state.resultsReducer
  }
}

export default connect(
  mapStateToProps
)(GiphyResults)


GiphyResults.propTypes = {
  url: PropTypes.string.isRequired, // URL to fetch from
  nextFetchLimit: PropTypes.number.isRequired, // Lazy load fetch quantity limit
  options: PropTypes.object, // Optional key/value params for the URL
};
