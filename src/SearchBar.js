import React, { Component } from 'react';
import './SearchBar.css';
import classNames from 'classnames';
import PropTypes from 'prop-types';

/*
User can enter a search term which fires an
event back to the caller. Events are throttled with
with a brief end of type delay.
*/
class SearchBar extends Component {

  constructor(props) {
    super(props);
    // changes to these vars should not cause re-render
    // so they are not in the state
    this.onChangeDelay = 400;
    this.timeoutHandle = 0;

    this.state = {
      focused: false

    };
  }

  onFocus() {
    this.setState({
      ...this.state,
      focused: true
    })
  }

  onBlur() {
    this.setState({
      ...this.state,
      focused: false
    })
  }

  handleChange(e) {
    const value = e.target.value;
    if (this.timeoutHandle !== 0) {
      // more input detected, restart the delay again
      clearTimeout(this.timeoutHandle);
    }
    // only fire the change event after a short delay of no typing.
    this.timeoutHandle = setTimeout(()=>{
      this.props.onChange(value.trim());
    }, this.onChangeDelay);
  }

  render() {
   return (
      <div className={classNames({
        'search-bar': true,
        'focused': this.state.focused
      })}>
        <input
          placeholder="search..."
          onBlur={this.onBlur.bind(this)}
          onFocus={this.onFocus.bind(this)}
          onChange={this.handleChange.bind(this)}
          type="text"
         />
      </div>
    );
  }
}

export default SearchBar

SearchBar.propTypes = {
  onChange: PropTypes.func.isRequired, // callback for text change event
};
