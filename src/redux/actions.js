import store from './store'


export function fetchAction(url, options) {
    var defaults = Object.assign({}, store.getState().fetchReducer.options);
    options = Object.assign(defaults, options);
    
    store.dispatch({
      type: 'REQUEST_FETCHEDING',
      options,
      url,
    });
    const composedUrl = Object.keys(options).reduce((a,v) => `${a}&${v}=${options[v]}`, url);

    store.dispatch((function() {
      return function (dispatch) {
        fetch(composedUrl)
          .then((response) => {
            response.json().then((json)=>{
              dispatch({
                type: 'REQUEST_FETCHED',
                response: json,
                options,
              });
            });
          })
          .catch((error)=>{
            dispatch({
                type: 'REQUEST_ERROR',
                message: error
              });
          })
      };
    }()));
}