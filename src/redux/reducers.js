import { combineReducers } from 'redux'


function fetchReducer(state = {}, action) {
  switch (action.type) {
      case 'REQUEST_FETCHEDING':
        return {
            ...state,
            url: action.url,
            mode: action.mode,
            options: action.options,
            fetchState: 'pending'
        }
      case 'REQUEST_FETCHED':
        return {
            ...state,
            fetchState: 'success'
        }
      case 'REQUEST_ERROR': 
        return {
            ...state,
            fetchState: 'error'
        }
      default:
        return state;
  }
}
  
function resultsReducer(state = {}, action) {
    switch (action.type) {
      case 'REQUEST_FETCHED':
        if (action.options.offset === 0) {
            // assume new list of results
            return {
            ...state,
            results: [action.response],
          }
        }
        return {
          // append to the list of results
          ...state,
          results: [...state.results, action.response],
        }
      default:
          return state;
    }
}


export default combineReducers({
  fetchReducer,
  resultsReducer
})

