import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import rootReducer from './reducers'

const store = createStore(rootReducer, {
      fetchReducer: {
        url: 'https://api.giphy.com/v1/gifs/trending?',
        options: {
          api_key: 'RZkIbRJV55mkDgRtIHHPulDanIoZOlrD',
          rating: 'G',
          lang: 'en',
          limit: 100,
          offset: 0,
          q: '',
        }
      },
      resultsReducer: {
        results: [],
      }
  },
  applyMiddleware(
    thunkMiddleware
));

export default store;
